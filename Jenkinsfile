pipeline {
    agent { label 'dockerAgent1' }
    environment {
        DATE = new Date().format('yy-MM-dd')
        TAG = "${DATE}-${BUILD_NUMBER}"
        APP_NAME="website"
        APP_PATH="demo_project"
        ENV_IMG_NAME="java17_mvn_env"
        REGISTRY_NAME="registry.licenta2024atm.com"
        REGISTRY_PROJECT_URL="registry.licenta2024atm.com/developer"
    }
    stages {
        stage('Checkout & Docker Image') {
            steps {
                git url: 'https://gitlab.com/catanoiusimona2013/test.git', branch: 'main'
                sh """docker build -t ${ENV_IMG_NAME}:latest ."""
            }
        }
        stage('Build application') {
            agent {
                docker {
                  label 'dockerAgent1'
                  image """${ENV_IMG_NAME}:latest"""
                }
            }
            stages {
                stage('Compile code') {
                    steps {
                        dir("${env.WORKSPACE}/${APP_PATH}") {
                            sh 'mvn compile'
                        }
                    }
                }
                stage('Run unit tests') {
                    steps {
                        dir("${env.WORKSPACE}/${APP_PATH}") {
                            sh 'mvn test'
                        }
                    }
                }
                stage('Create artifact') {
                    steps {
                        dir("${env.WORKSPACE}/${APP_PATH}") {
                            sh 'mvn package'
                        }
                    }
                }
                stage('Stash artifact') {
                    steps {
                        dir("${env.WORKSPACE}/${APP_PATH}") {
                            stash includes: 'target/*.jar', name: 'jar_files' 
                        }
                    }
                }
            }
        }
        stage('Create container image') {
            steps {
                dir("${env.WORKSPACE}/${APP_PATH}") {
                    unstash 'jar_files'
                    withCredentials([usernamePassword(credentialsId: "website_registry_deploy", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh """docker login ${REGISTRY_NAME} -u ${USERNAME} -p ${PASSWORD}"""
                    }
                    sh """docker build -t ${REGISTRY_PROJECT_URL}/${APP_NAME}:${TAG} ."""
                }
            }
        }
        stage('Push image to registry'){
            steps {
                sh """docker push ${REGISTRY_PROJECT_URL}/${APP_NAME}:${TAG}"""
            }
        post {
            always {
                    sh """docker image rm ${REGISTRY_PROJECT_URL}/${APP_NAME}:${TAG}"""
                }
            }
        }
    }
}
