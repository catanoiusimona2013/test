package licenta.atm.my_site;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    public void setUp()
    {
        calculator = new Calculator();
    }

    @Test
    public void TestMutiply()
    {
        assertEquals(20, calculator.multiply(4, 5));
    }
}
