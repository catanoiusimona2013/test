# Start with Alpine base image
FROM alpine:latest

# Set environment variables
ENV MAVEN_HOME=/opt/maven \
    JAVA_HOME=/usr/lib/jvm/java-17-openjdk

# Install OpenJDK 17, Maven, Git
RUN apk --no-cache add openjdk17 maven git

# Set working directory
WORKDIR /home

# Default command
CMD ["sh"]